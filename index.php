<?php require_once('Connections/connmyapps.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['txtuser'])) {
  $loginUsername=$_POST['txtuser'];
  $password=$_POST['txtpassword'];
  $MM_fldUserAuthorization = "leveluser";
  $MM_redirectLoginSuccess = "datapegawai.php?pesan=Login Sukses";
  $MM_redirectLoginFailed = "index.php?pesan=Gagal Login";
  $MM_redirecttoReferrer = false;
  mysql_select_db($database_connmyapps, $connmyapps);
  	
  $LoginRS__query=sprintf("SELECT iduser, passuser, leveluser FROM tblogin WHERE iduser=%s AND passuser=%s",
  GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $connmyapps) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
    
    $loginStrGroup  = mysql_result($LoginRS,0,'leveluser');
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include("bootstrap.php"); ?>


<title>Welcome, Please Login</title>
</head>

<body>
<div class="container">
  <div class="jumbotron">
    <h1>HR Login</h1>
    <h2>CV. DUTA PURNAMA</h2>
  </div>
  <form action="<?php echo $loginFormAction; ?>" method="POST" name="flogin" id="flogin">
    <table width="100%" border="0" cellspacing="2" cellpadding="2">
      <tr>
        <td width="17%">User</td>
        <td width="2%">:</td>
        <td width="81%"><input name="txtuser" type="text" id="txtuser" size="30" class="form-control"></td>
      </tr>
      <tr>
        <td>Password</td>
        <td>:</td>
        <td><input name="txtpassword" type="password" id="txtpassword" size="30" maxlength="10" class="form-control"></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><input type="submit" name="button" id="button" value="Login" class="btn btn-primary">
          <span class="error"><?php if(!empty($_GET['pesan'])) { echo $_GET['pesan']; } ?></span></td>
      </tr>
    </table>
  </form>
  <p>&nbsp;</p>
</div>
</body>
</html>